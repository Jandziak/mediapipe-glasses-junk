import { Camera } from "@mediapipe/camera_utils";
//import { FaceMesh } from "@tensorflow-models/facemesh";
import { FaceMesh } from "@mediapipe/face_mesh";
import "@tensorflow/tfjs-backend-wasm";
import * as dat from "dat.gui";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import Stats from "three/examples/jsm/libs/stats.module.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";
import { CSS2DObject } from "three/examples/jsm/renderers/CSS2DRenderer.js";
import "./style.css";
let cameraReady = false;
//tf.setBackend("wasm").then(() => main());
//Video camera
const videoElement = document.getElementsByClassName("input_video")[0];

function onResults(results) {
  if (results.multiFaceLandmarks) {
    for (const landmarks of results.multiFaceLandmarks) {
      var j = landmarks[100];
    }
  }

  //console.log(j);
  glasses.position.set((j.x - 0.46) * 7.42 * 2, (0.6 - j.y) * 4.2 * 2, 1);
  render2();
}

const faceMesh = new FaceMesh({
  locateFile: (file) => {
    return `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh@0.1/${file}`;
  },
});
faceMesh.onResults(onResults);

// Instantiate a camera. We'll feed each frame we receive into the solution.
const video_camera = new Camera(videoElement, {
  onFrame: async () => {
    await faceMesh.send({ image: videoElement });
  },
  width: 1280,
  height: 720,
});
video_camera.start();
// Debug
const gui = new dat.GUI();

// Canvas
const canvas = document.querySelector("canvas.webgl");

// Scene
const scene = new THREE.Scene();
scene.background = new THREE.Color(0xaec6cf);

// Textures
const envTexture = new THREE.CubeTextureLoader().load([
  "img/px_25.jpeg",
  "img/nx_25.jpeg",
  "img/py_25.jpeg",
  "img/ny_25.jpeg",
  "img/pz_25.jpeg",
  "img/nz_25.jpeg",
]);
envTexture.mapping = THREE.CubeReflectionMapping;

// Materials
const cubeRenderTarget = new THREE.WebGLCubeRenderTarget(2048);

const refractionMaterial = new THREE.MeshPhysicalMaterial({
  metalness: 0,
  roughness: 0.2,
  envMap: envTexture,
  refractionRatio: 1.0,
  transparent: true,
  transmission: 0.99,
  side: THREE.DoubleSide,
  clearcoat: 1.0,
  clearcoatRoughness: 0.39,
});
const armMaterial = new THREE.MeshPhysicalMaterial({
  color: 0x444444,
  envMap: envTexture,
  metalness: 1,
  roughness: 0,
});
const hingMaterial = new THREE.MeshPhysicalMaterial({
  color: 0xf0e68c,
  envMap: envTexture,
  metalness: 1,
  roughness: 0,
});
const frameRefraction = new THREE.MeshPhysicalMaterial({
  metalness: 1,
  roughness: 0,
  color: 0xffffff,
  envMap: cubeRenderTarget.texture,
  refractionRatio: 0.9,
  transparent: true,
  transmission: 0.0,
  side: THREE.FrontSide,
});
const frameMaterial = new THREE.MeshPhysicalMaterial({
  color: 0xb2ffc8,
  envMap: envTexture,
  metalness: 1,
  roughness: 0,
  transparent: true,
  transmission: 0.66,
  side: THREE.BackSide,
});
cubeRenderTarget.texture.mapping = THREE.CubeRefractionMapping;

// Glasses
let modelReady = false;
let glasses = new THREE.Object3D();
const glTFLoader = new GLTFLoader();

let annotationCounter = 0;
let annotations = {};
const annotationMarkers = [];
glTFLoader.load(
  "models/glasses.glb",
  (gltf) => {
    gltf.scene.traverse(function (child) {
      if (child.isMesh) {
        const mesh = child.clone();
        mesh.rotation.x = 0;
        if (mesh.name.endsWith("Lens")) {
          mesh.material = refractionMaterial;
          glasses.add(mesh);
        } else if (mesh.name.endsWith("hinge")) {
          mesh.material = hingMaterial;
          glasses.add(mesh);
        } else if (mesh.name.endsWith("frame")) {
          mesh.material = frameRefraction;
          glasses.add(mesh);
          const newMesh = new THREE.Mesh(mesh.geometry.clone(), frameMaterial);
          newMesh.rotation.copy(mesh.rotation);
          glasses.add(newMesh);
        } else if (mesh.name.endsWith("Arm")) {
          mesh.material = armMaterial;
          glasses.add(mesh);
        }
      } else if (child.isObject3D) {
        if (child.name.startsWith("Annotation")) {
          const aId = (annotationCounter++).toString();
          annotations[aId] = {
            title: child.userData.title,
            description: child.userData.description,
          };
          const annotationSpriteMaterial = new THREE.SpriteMaterial({
            depthTest: false,
            depthWrite: false,
            sizeAttenuation: false,
          });
          const annotationSprite = new THREE.Sprite(annotationSpriteMaterial);
          annotationSprite.scale.set(6.6, 6.6, 6.6);
          // changing from blender annotation coords to threejs coords
          const tmpY = -child.position.y;
          child.position.y = child.position.z;
          child.position.z = tmpY;

          annotationSprite.userData.id = aId;
          annotationSprite.layers.set(1);
          annotationMarkers.push(annotationSprite);
          const annotationDiv = document.createElement("div");
          annotationDiv.className = "annotationLabel";
          annotationDiv.innerHTML = aId;
          const annotationLabel = new CSS2DObject(annotationDiv);
          annotationLabel.position.copy(annotationSprite.position);
          glasses.add(annotationLabel);
          if (child.userData.title) {
            const annotationTextDiv = document.createElement("div");
            annotationTextDiv.className = "annotationDescription";
            annotationTextDiv.innerHTML = child.userData.title;
            if (child.userData.description) {
              annotationTextDiv.innerHTML +=
                "<p>" + child.userData.description + "</p>";
            }
            annotationDiv.appendChild(annotationTextDiv);
            annotations[aId].descriptionDomElement = annotationTextDiv;
          }
        }
      }
    });
    glasses.scale.set(0.01, 0.01, 0.01);
    scene.add(glasses);
    glasses.position.set(0, 1, 0);
    glasses.rotation.set(0.5 * Math.PI, 0, 0);
    modelReady = true;
  },
  (xhr) => {
    console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
  },
  (error) => {
    console.log(error);
  }
);

// Lights
const pointLight = new THREE.PointLight(0xffffff, 0.1);
pointLight.position.x = 2;
pointLight.position.y = 3;
pointLight.position.z = 4;
scene.add(pointLight);
console.log(111111);
/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
);
camera.position.set(0, 1, 5);
scene.add(camera);

// Controls
const lensFolder = gui.addFolder("Lenses");
lensFolder.add(refractionMaterial, "opacity", 0, 1.0, 0.01).name("Opacity");
lensFolder.add(refractionMaterial, "metalness", 0, 1.0, 0.01).name("Metalness");
lensFolder.add(refractionMaterial, "roughness", 0, 1.0, 0.01).name("Roughness");
lensFolder
  .add(refractionMaterial, "transmission", 0, 1.0, 0.01)
  .name("Transmission");
lensFolder.add(refractionMaterial, "clearcoat", 0, 1.0, 0.01).name("Clearcoat");
lensFolder
  .add(refractionMaterial, "clearcoatRoughness", 0, 1.0, 0.01)
  .name("ClearcoatRoughness");
lensFolder.open();
const framesFolder = gui.addFolder("Frames");
framesFolder.add(frameRefraction, "opacity", 0, 1.0, 0.01).name("Opacity");
framesFolder.add(frameRefraction, "metalness", 0, 1.0, 0.01).name("Metalness");
framesFolder.add(frameRefraction, "roughness", 0, 1.0, 0.01).name("Roughness");
framesFolder
  .add(frameRefraction, "transmission", 0, 1.0, 0.01)
  .name("Transmission");
framesFolder
  .add(frameRefraction, "refractionRatio", 0, 1.0, 0.01)
  .name("RefractionRatio");
framesFolder.open();
const locationFolder = gui.addFolder("Location");
var obj = {
  "Selfie Mode": function () {
    console.log("clicked");
  },
};
locationFolder.add(obj, "Selfie Mode");
locationFolder.add(glasses.position, "x", -10, 10, 0.01).name("x");
locationFolder.add(glasses.rotation, "y", -10, 10, 0.01).name("x");
locationFolder.add(glasses.rotation, "z", -10, 10, 0.01).name("x");
locationFolder.add(glasses.rotation, "x", -10, 10, 0.01).name("x");
locationFolder.open();
// stats
const stats = Stats();
document.body.appendChild(stats.dom);

cubeRenderTarget.texture.mapping = THREE.CubeRefractionMapping;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({ canvas: canvas });
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.sortObjects = false;

// document.body.appendChild(renderer.domElement);

// Controls
const controls = new OrbitControls(camera, renderer.domElement);
controls.target.set(0, 0.5, 0);
controls.addEventListener("change", function () {
  if (camera.position.y < 0.1) {
    camera.position.y = 0.1;
  }
});
const cubeCamera = new THREE.CubeCamera(0.1, 100, cubeRenderTarget);
scene.add(cubeCamera);

/**
 * Animate
 */
var animate = function () {
  window.requestAnimationFrame(animate);
  controls.update();
  let delta = clock.getDelta();
  if (delta > 0.1) delta = 0.1;
  if (modelReady) {
  }
  render();
  stats.update();
};
var render2 = function () {
  if (modelReady) {
    requestAnimationFrame(render);
    renderer.render(scene, camera_);
    main();
  }
};
function render() {
  if (modelReady) {
    cubeCamera.position.copy(camera.position);
    camera.layers.disable(1);
    cubeCamera.update(renderer, scene);
    camera.layers.enable(1);
  }
  renderer.render(scene, camera);
}
const clock = new THREE.Clock();

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});
